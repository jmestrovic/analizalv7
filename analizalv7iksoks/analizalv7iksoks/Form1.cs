﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace analizalv7iksoks
{
    public partial class Form1 : Form
    {
        bool turn = true; // true prvi igrac x
        int counter;
        public Form1()
        {
            InitializeComponent();
        }
        private void mouse_click(object sender, MouseEventArgs e)
        {
            Button but = (Button)sender;
            if (turn) { but.Text = "X"; }
            else { but.Text = "O"; }
            turn = !turn;
            but.Enabled = false;
            check();
            if (!turn)label_pTurn.Text = label_p2.Text;
            else label_pTurn.Text = label_p1.Text;
            counter++;
            if (counter == 9) MessageBox.Show("Nerjeseno je");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            label_p1.Text = textBox1.Text;
            label_p2.Text = textBox2.Text;
            label_pTurn.Text = label_p1.Text;
        }
        private void check()
        {
            bool winner=false;
            if ((button1.Text == button2.Text) && (button2.Text == button3.Text) && (!button1.Enabled)) winner = true;
            else if ((button4.Text == button5.Text) && (button5.Text == button6.Text) && (!button4.Enabled)) winner = true;
            else if ((button7.Text == button8.Text) && (button8.Text == button9.Text) && (!button7.Enabled)) winner = true;
            else if ((button1.Text == button3.Text) && (button3.Text == button7.Text) && (!button1.Enabled)) winner = true;
            else if ((button2.Text == button5.Text) && (button5.Text == button8.Text) && (!button2.Enabled)) winner = true;
            else if ((button3.Text == button6.Text) && (button6.Text == button9.Text) && (!button3.Enabled)) winner = true;
            else if ((button1.Text == button5.Text) && (button5.Text == button9.Text) && (!button1.Enabled)) winner = true;
            else if ((button3.Text == button5.Text) && (button5.Text == button7.Text) && (!button3.Enabled)) winner = true;
            if (winner)
            {
                if (!turn) MessageBox.Show(label_p1.Text + " je pobjednik!");
                else MessageBox.Show(label_p2.Text + " je pobjednik!");
            }
        }
    }
}
