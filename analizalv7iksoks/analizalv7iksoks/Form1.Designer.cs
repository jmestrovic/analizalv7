﻿namespace analizalv7iksoks
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label_p1 = new System.Windows.Forms.Label();
            this.label_p2 = new System.Windows.Forms.Label();
            this.label_pTurn = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Igraci";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(0, 43);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(0, 71);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(285, 95);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(59, 58);
            this.button1.TabIndex = 3;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouse_click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(350, 95);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(59, 58);
            this.button2.TabIndex = 4;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouse_click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(415, 95);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(59, 58);
            this.button3.TabIndex = 5;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouse_click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(285, 159);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(59, 58);
            this.button4.TabIndex = 6;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouse_click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(350, 159);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(59, 58);
            this.button5.TabIndex = 7;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouse_click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(415, 159);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(59, 58);
            this.button6.TabIndex = 8;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouse_click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(285, 223);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(59, 58);
            this.button7.TabIndex = 9;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouse_click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(350, 223);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(59, 58);
            this.button8.TabIndex = 10;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouse_click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(415, 223);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(59, 58);
            this.button9.TabIndex = 11;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouse_click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(0, 112);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(130, 42);
            this.button10.TabIndex = 12;
            this.button10.Text = "Dodaj igrace";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "Prvi igrac";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(336, 311);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Na potezu";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 15;
            this.label4.Text = "Drugi igrac";
            // 
            // label_p1
            // 
            this.label_p1.AutoSize = true;
            this.label_p1.Location = new System.Drawing.Point(12, 227);
            this.label_p1.Name = "label_p1";
            this.label_p1.Size = new System.Drawing.Size(13, 17);
            this.label_p1.TabIndex = 16;
            this.label_p1.Text = "-";
            // 
            // label_p2
            // 
            this.label_p2.AutoSize = true;
            this.label_p2.Location = new System.Drawing.Point(12, 286);
            this.label_p2.Name = "label_p2";
            this.label_p2.Size = new System.Drawing.Size(13, 17);
            this.label_p2.TabIndex = 17;
            this.label_p2.Text = "-";
            // 
            // label_pTurn
            // 
            this.label_pTurn.AutoSize = true;
            this.label_pTurn.Location = new System.Drawing.Point(347, 339);
            this.label_pTurn.Name = "label_pTurn";
            this.label_pTurn.Size = new System.Drawing.Size(13, 17);
            this.label_pTurn.TabIndex = 18;
            this.label_pTurn.Text = "-";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label_pTurn);
            this.Controls.Add(this.label_p2);
            this.Controls.Add(this.label_p1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Iks oks";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_p1;
        private System.Windows.Forms.Label label_p2;
        private System.Windows.Forms.Label label_pTurn;
    }
}

